Name:           pkcs11-helper
Version:        1.30.0
Release:        1
Summary:        A library that simplifies the interaction with PKCS#11 providers
License:        BSD and GPLv2
URL:            https://github.com/OpenSC/OpenSC/wiki
Source0:        https://github.com/OpenSC/%{name}/releases/download/%{name}-%{version}/%{name}-%{version}.tar.bz2

Patch1:         pkcs11-helper-rfc7512.patch

BuildRequires:	gcc openssl-devel doxygen

%description
PKCS#11 is published standard. PKCS#11 is the de-facto standard to access cryptographic devices.
Pkcs11-helper is a library that simplifies the interaction with PKCS#11 providers for end-user
applications.
Pkcs11-helper allows using multiple PKCS#11 providers at the same time, enumerating available token
certificates, or selecting a certificate directly by serialized id, handling card removal and card
insert events, handling card re-insert to a different slot, supporting session expiration and much
more all using a simple API.
Pkcs11-helper is not designed to manage card content, since object attributes are usually vendor
specific, and 99% of application need to access existing objects in order to perform signature and
decryption.

%package devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       automake openssl-devel

%description devel
This package contains the development files for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1


%build
%configure --enable-doc --disable-static
%make_build

%install
%make_install

mkdir apidocdir
mv $RPM_BUILD_ROOT%{_datadir}/doc/%{name}/api/ apidocdir/
rm -rf $RPM_BUILD_ROOT%{_datadir}/doc/%{name}/

%delete_la_and_a

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc AUTHORS ChangeLog README THANKS
%license COPYING*
%{_libdir}/lib%{name}.so.*

%files devel
%doc apidocdir/*
%{_includedir}/%{name}-1.0/
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/lib%{name}-1.pc
%{_datadir}/aclocal/%{name}-1.m4

%files help
%doc THANKS
%{_mandir}/man8/*

%changelog
* Mon May 13 2024 yaoxin <yao_xin001@hoperun.com> - 1.30.0-1
- Upgrade to 1.30.0
- add dynamic loader provider attribute
- support DSA in libressl-3.5.0
- fix openssl_ex_data_dup prototype

* Thu Feb 09 2023 yaoxin <yaoxin30@h-partners.com> - 1.29.0-1
- Update to 1.29.0

* Fri Feb 03 2023 yaoxin <yaoxin30@h-partners.com> - 1.25.1-2
- Fix build error due to openssl update to 3.0

* Thu Nov 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.25.1-1
- Package init
